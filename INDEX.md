# FDNet

The *FDNet Package* consists of various open source licensed programs. Those
programs are not part of FDNet. Nor are those programs under the License
as FDNet. FDNet is simply a batch script that talks to programs in order to
provide networking support on some instances of FreeDOS.

For your convenience, the FDNet package does include the sources for the
specific versions of many of the utilities used. For the latest versions of
those utilities, you should visit their website.

See the [License file](LICENSE) file for more information
# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## FDNET.LSM

<table>
<tr><td>title</td><td>FDNet</td></tr>
<tr><td>version</td><td>2024-12-20</td></tr>
<tr><td>entered&nbsp;date</td><td>2024-12-20</td></tr>
<tr><td>description</td><td>Basic networking support package</td></tr>
<tr><td>summary</td><td>Basic networking support package for FreeDOS. Provides basic networking support for FreeDOS on some supported hardware/virtual platforms. FDNet is originally based on Rugxulo's MetaDOS CONNECT batch file.</td></tr>
<tr><td>keywords</td><td>dos batch net</td></tr>
<tr><td>author</td><td>Shidel and Rugxulo</td></tr>
<tr><td>maintained&nbsp;by</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://gitlab.com/FreeDOS/net/fdnet</td></tr>
<tr><td>alternate&nbsp;site</td><td>https://fd.lod.bz/repos/current/pkg-html/fdnet.html</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[Various open source licenses, see LICENSE file](LICENSE)</td></tr>
</table>
