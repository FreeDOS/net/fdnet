The *FDNet Package* consists of various open source licensed programs. Those
programs are not part of FDNet. Nor are those programs under the License
as FDNet. FDNet is simply a batch script that talks to programs in order to
provide networking support on some instances of FreeDOS.

For your convenience, the FDNet package does include the sources for the
specific versions of many of the utilities used. For the latest versions of
those utilities, you should visit their website.

See the [License file](LICENSE) file for more information